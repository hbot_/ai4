import matplotlib.pyplot as plt
import math
import time

data_file = open('laboratory.dat', 'r+')

data = [int(line.strip()) for line in data_file]
energy_list = []
magnitude_list = []

sample_number = len(data)

sampling_time = 0.3
sampling_rate = sample_number / sampling_time
window_time = 0.03
window_size = int (sampling_rate * window_time)


def window(n, num_of_samples):
    if  (n >= 0 and n <= num_of_samples - 1):
        return 1
    else:
        return 0

def sign(n):
    if (n >= 0):
        return 1
    else:
        return 0

def energy(n):
    value = 0
    for k in range(n - window_size + 1, n + 1):
        if (k >= 0 and k < sample_number):
          value += int(data[k]) * int(data[k]) * window(n - k, sample_number)
    return value

def magnitude(n):
    value = 0
    for k in range (n - window_size + 1, n + 1):
        if (k >= 0 and k < sample_number):
            value += math.fabs(data[k]) * window (n - k, sample_number)
    return value

def zcr(n):
    value = 0
    for k in range (n - window_size + 1, n + 1):
        if (k > 0 and k < sample_number):
            value += math.fabs(sign(data[k]) - sign(data[k-1])) * \
                     window(n-k, sample_number)
    return (value / (2 * window_size))

#Computing values

max_val = 0
for i in range (sample_number):
  val = energy(i)
  if (val > max_val):
    max_val = val
  energy_list.append(val)

max_mag = 0
mag_list = []
for i in range (sample_number):
  val = magnitude(i)
  if (val > max_mag):
    max_mag = val
  mag_list.append(val)

max_zcr = 0
zcr_list = []
for i in range (sample_number):
  val = zcr(i)
  if (val > max_zcr):
    max_zcr = val
  zcr_list.append(val)

max_auto = 0
lag = 0

#Normalization
for i in range(len(energy_list)):
        energy_list[i] /= (max_val * 1.0)

for i in range(len(mag_list)):
  mag_list[i] /= (max_mag * 1.0)

for i in range (len(zcr_list)):
  zcr_list[i] /= (max_zcr * 1.0)

#for i in range(len(auto_list)):
#  auto_list[i] /= (max_auto * 1.0)

max_data = max(data)
for i in range(len(data)):
  data[i] /= (max_data * 1.0)

#Plot data
plt.step(range(0, len(energy_list), 1), energy_list, label='Energy')
plt.step(range(0, len(data), 1), data, label='Signal')
plt.step(range(0, len(mag_list), 1), mag_list, label='Magnitude')
plt.step(range(0, len(mag_list), 1), zcr_list, label='Zcr')

plt.legend()
plt.show()

print "Sampling rate: ", len(data) / sampling_time
