import matplotlib.pyplot as plt
import math
import time
import csv

speech_energy = []
speech_magnitude = []
speech_zcr = []

silence_energy = []
silence_magnitude = []
silence_zcr = []

def readValues (filename, type):
  with open (filename, "rb") as csvfile:
    reader = csv.reader (csvfile, delimiter = ',')
    for row in reader:
      if type == "speech":
        speech_energy.append(float(row[0]))
        speech_magnitude.append(float(row[1]))
        speech_zcr.append(float(row[2]))
      else:
        silence_energy.append(float(row[0]))
        silence_magnitude.append(float(row[1]))
        silence_zcr.append(float(row[2]))
 
readValues("./values/speech_data.csv", "speech")
readValues("./values/silence_data.csv", "silence")

#print speech_energy
fig = plt.figure()
ax1 = fig.add_subplot(111)

#x = range(100)
#y = range(100,200)

ax1.scatter(speech_energy, speech_magnitude, s=10, c='b', marker="s", label='Speech')
ax1.scatter(silence_energy, silence_magnitude, s=10, c='r', marker="o", label='Silence')
ax1.set_xlabel('Energy')
ax1.set_ylabel('Magnitude')
ax1.legend()
plt.show()
      

