import math
import time
import csv

prob_speech = 0.5
prob_silence = 0.5

speech_energy = []
speech_magnitude = []
speech_zcr = []

silence_energy = []
silence_magnitude = []
silence_zcr = []

def truncateList(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

def computeMean (aList):
  return sum(aList) / float(len(aList))

def computeVariance (aList):
  mean = computeMean(aList)
  varianceMean = 0
  for i in range (len(aList)):
    varianceMean += math.pow(aList[i] - mean, 2)
  return varianceMean / len(aList)

def computeGaussian (value, mean, variance):
  factor = 1 / math.sqrt ( 2 * math.pi * variance )
  exponent = ((-1) * math.pow (value - mean, 2)) / (2 * variance)
  
  return factor * math.exp (exponent)

def readValues (filename, type):
  with open (filename, "rb") as csvfile:
    reader = csv.reader (csvfile, delimiter = ',')
    for row in reader:
      if type == "speech":
        speech_energy.append(float(row[0]))
        speech_magnitude.append(float(row[1]))
        speech_zcr.append(float(row[2]))
      else:
        silence_energy.append(float(row[0]))
        silence_magnitude.append(float(row[1]))
        silence_zcr.append(float(row[2]))

readValues ("./values/speech_data.csv", "speech")
readValues ("./values/silence_data.csv", "silence")

def varianceTrainer(aListOfLists, testSliceNumber):
  variance = 0
  for i in range(len(aListOfLists)):
    if (i != testSliceNumber):
      variance += computeVariance(aListOfLists[i])
  return variance / float (len ( aListOfLists ) - 1) # -1 because we test on one slice

def meanTrainer(aListOfLists, testSliceNumber):
  mean = 0
  for i in range(len(aListOfLists)):
    if (i != testSliceNumber):
      mean += computeMean(aListOfLists[i])
  return mean / float (len ( aListOfLists ) - 1) # -1 because we test on one slice

def computeProbability (initProb, energy, magnitude, zcr, 
    energy_mean, energy_variance, 
    magnitude_mean, magnitude_variance, 
    zcr_mean, zcr_variance):
  normalD_energy = computeGaussian(energy, energy_mean, energy_variance)  
  normalD_magnitude = computeGaussian(magnitude, magnitude_mean, magnitude_variance)
  normalD_zcr = computeGaussian(zcr, zcr_mean, zcr_variance)
  return initProb * normalD_energy * normalD_magnitude * normalD_zcr

#truncateList splits a list into a list of lists of size 5. 
#A list of size 50, will be split into 10 lists of size 5

silence_energy = truncateList(silence_energy[::-1], 5)
silence_magnitude = truncateList(silence_magnitude[::-1], 5)
silence_zcr = truncateList(silence_zcr[::-1], 5)

speech_energy = truncateList(speech_energy[::-1], 5)
speech_magnitude = truncateList(speech_magnitude[::-1], 5)
speech_zcr = truncateList(speech_zcr[::-1], 5)

accurate_speech = 0
accurate_silence = 0

for i in range(10):
  f_accurate_speech = 0
  f_accurate_silence = 0

  silence_energy_mean = meanTrainer(silence_energy, i)
  silence_magnitude_mean = meanTrainer(silence_magnitude, i)
  silence_zcr_mean = meanTrainer(silence_zcr, i)

  silence_energy_variance = varianceTrainer(silence_energy, i)
  silence_magnitude_variance = varianceTrainer(silence_magnitude, i)
  silence_zcr_variance = varianceTrainer(silence_zcr, i)

  speech_energy_mean = meanTrainer(speech_energy, i)
  speech_magnitude_mean = meanTrainer(speech_magnitude, i)
  speech_zcr_mean = meanTrainer(speech_zcr, i)

  speech_energy_variance = varianceTrainer(speech_energy, i)
  speech_magnitude_variance = varianceTrainer(speech_magnitude, i)
  speech_zcr_variance = varianceTrainer(speech_zcr, i)

  for j in range(len(speech_energy[i])):
    speech_prob = computeProbability(prob_speech, speech_energy[i][j], speech_magnitude[i][j], speech_zcr[i][j],
                                     speech_energy_mean, speech_energy_variance,
                                     speech_magnitude_mean, speech_magnitude_variance,
                                     speech_zcr_mean, speech_zcr_variance)
    silence_prob = computeProbability(prob_silence, speech_energy[i][j], speech_magnitude[i][j], speech_zcr[i][j],
                                     silence_energy_mean, silence_energy_variance,
                                     silence_magnitude_mean, silence_magnitude_variance,
                                     silence_zcr_mean, silence_zcr_variance)

    sp = speech_prob / (speech_prob + silence_prob)
    sl = silence_prob / (speech_prob + silence_prob)

    if (sp > sl):
      accurate_speech += 1
      f_accurate_speech += 1

    print ('speech_%(file)02d  speech_probability: %(sp).5f --- silence probability: %(sl).5f' % {"file": i * 5 + j + 1, "sp": sp, "sl": sl})

  for j in range(len(silence_energy[i])):
    speech_prob = computeProbability(prob_speech, silence_energy[i][j], silence_magnitude[i][j], silence_zcr[i][j],
                                     speech_energy_mean, speech_energy_variance,
                                     speech_magnitude_mean, speech_magnitude_variance,
                                     speech_zcr_mean, speech_zcr_variance)
    silence_prob = computeProbability(prob_silence, silence_energy[i][j], silence_magnitude[i][j], silence_zcr[i][j],
                                     silence_energy_mean, silence_energy_variance,
                                     silence_magnitude_mean, silence_magnitude_variance,
                                     silence_zcr_mean, silence_zcr_variance)

    sp = speech_prob / (speech_prob + silence_prob)
    sl = silence_prob / (speech_prob + silence_prob)

    if (sl > sp):
      accurate_silence +=1
      f_accurate_silence += 1

    print ('silence_%(file)02d speech_probability: %(sp).5f --- silence probability: %(sl).5f' % {"file": i * 5 + j + 1, "sp": sp, "sl": sl})

  print "****************"
  print ("Test fold number %02d" % (i + 1))
  print ("Fold speech accuracy  %.2f%%" % (float(f_accurate_speech) * 2 * 10))
  print ("Fold silence accuracy %.2f%%" % (float(f_accurate_silence) * 2 * 10))
  print ("Overall fold accuracy %.2f%%" % (float(f_accurate_speech + f_accurate_silence) * 10))
  print "****************"

print ("*************************")
print ("Speech accuracy %.2f%%" % (float(accurate_speech) / 50 * 100))
print ("Silence accuracy %.2f%%" % (float(accurate_silence) / 50 * 100))
