import math
import time
import glob


sample_number = 2400

sampling_time = 0.3
sampling_rate = sample_number / sampling_time

window_time   = 0.03
window_size   = int (sampling_rate * window_time)

silence_files = [] #glob.glob("./silence_speech/silence_??.dat")
speech_files  = [] #glob.glob("./silence_speech/speech_??.dat")

for i in range(1, 51):
  silence_files.append('./silence_speech/silence_%02d.dat' % (i, ))
  speech_files.append('./silence_speech/speech_%02d.dat' % (i, ))

def window(n, num_of_samples):
    if  (n >= 0 and n <= num_of_samples - 1):
        return 1
    else:
        return 0

def sign(n):
    if (n >= 0):
        return 1
    else:
        return 0

def energy(n, data):
    value = 0
    for k in range(n - window_size + 1, n + 1):
        if (k >= 0 and k < sample_number):
          value += int(data[k]) * int(data[k]) * window(n - k, sample_number)
    return value

def magnitude(n, data):
    value = 0
    for k in range (n - window_size + 1, n + 1):
        if (k >= 0 and k < sample_number):
            value += math.fabs(data[k]) * window (n - k, sample_number)
    return value

def zcr(n, data):
    value = 0
    for k in range (n - window_size + 1, n + 1):
        if (k > 0 and k < sample_number):
            value += math.fabs(sign(data[k]) - sign(data[k-1])) * window(n-k, sample_number)
    return value / (2 * window_size)

def computeValues (input_files, output_file):
 
  out_file = open(output_file, "w")

  for _file in input_files:

    in_file   = open (_file)
    data      = [int(line.strip()) for line in in_file]

    e = 0
    m = 0
    z = 0

    print _file

    for i in range (sample_number):
      e += energy(i, data)
      m += magnitude(i, data)
      z += zcr(i, data)
   
    out_file.write('%f,%f,%f\n' % (math.log(e), math.log(m), z / sample_number))

computeValues(silence_files, "silence_data.csv")
computeValues(speech_files, "speech_data.csv")

